package sv.edu.bitlab.desafio.gilberto

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.graphics.toColorInt

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FormFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FormFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FormFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var name: EditText
    private lateinit var email: EditText
    private lateinit var sendButton: Button
    private lateinit var succesContainer: ConstraintLayout
    private lateinit var formContainer: ConstraintLayout
    private lateinit var seeCollection: TextView
    private lateinit var spinner: Spinner
    private lateinit var phoneNumer: EditText
    private var textSpinner: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.form_fragment, container, false)
        name = view.findViewById(R.id.fullName_editText)
        email = view.findViewById(R.id.email_editText)
        succesContainer = view.findViewById(R.id.success_container)
        formContainer = view.findViewById(R.id.form_container)
        seeCollection = view.findViewById(R.id.see_collection_textview)
        spinner = view.findViewById(R.id.findUs_spinner)
        phoneNumer = view.findViewById(R.id.phoneNumber_editText)

        formContainer.visibility = View.VISIBLE
        succesContainer.visibility = View.GONE

        sendButton = view.findViewById(R.id.sendForm_button)

        sendButton.setOnClickListener {
            listener?.checkFields(name, email, formContainer, succesContainer, textSpinner, phoneNumer)
        }

        seeCollection.setOnClickListener {
            listener?.showCollection()
        }

        ArrayAdapter.createFromResource(context!!, R.array.options_array, android.R.layout.simple_list_item_1).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d("SPINNER_STATUS", "Spinner is in onNothingSelected")
            }

            @SuppressLint("SetTextI18n")
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    Toast.makeText(context, "Seleccionado: num de elemnto 0}", Toast.LENGTH_SHORT).show()
                    textSpinner = "No selecciono ningun campo"

                } else {
                    Toast.makeText(context, "Seleccionado: ${parent?.getItemAtPosition(position).toString()}", Toast.LENGTH_SHORT).show()
                    textSpinner = parent?.getItemAtPosition(position).toString()
                }
            }
        }

        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
//    fun onButtonPressed() {
//        listener?.checkFields()
//    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onStop() {
        super.onStop()
        listener?.changeFragment()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("FRAGMENT_CYCLE", "The fragment is destroy")
        listener?.changeFragment()
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun checkFields(name: EditText, email: EditText, formContainer: ConstraintLayout, successContainer: ConstraintLayout, spinner: String, phone: EditText)
        fun changeFragment()
        fun showCollection()
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
