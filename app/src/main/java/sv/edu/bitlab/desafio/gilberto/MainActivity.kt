package sv.edu.bitlab.desafio.gilberto

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import sv.edu.bitlab.desafio.gilberto.R
import java.util.*

class MainActivity : AppCompatActivity(), FormFragment.OnFragmentInteractionListener, CollectionViewFragment.OnFragmentInteractionListener {

    private lateinit var checkEmail: EditText
    private lateinit var checkName: EditText
    private lateinit var chechPhoneNumber: EditText
    private lateinit var checkAboutUsSpinner: String
    private lateinit var checkForm: ConstraintLayout
    private lateinit var checkSucces: ConstraintLayout
    private val manager = supportFragmentManager
    private lateinit var handler: Handler
    private var imageURL: String = ""
    private var number = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handler = Handler()
        showFragmentOne()
    }


    override fun checkFields(name: EditText, email: EditText, formContainer: ConstraintLayout, successContainer: ConstraintLayout, spinner: String, phone: EditText) {
        checkName = name
        checkEmail = email
        chechPhoneNumber = phone
        checkAboutUsSpinner = spinner
        checkForm = formContainer
        checkSucces = successContainer

        val nameText = checkName.text.toString()
        val emailText = checkEmail.text.toString()
        val phoneText = chechPhoneNumber.text.toString()
        val spinnerText =checkAboutUsSpinner

        if (nameText.isNotEmpty() && emailText.isNotEmpty()){
            Toast.makeText(applicationContext, "Todo bien puede seguir", Toast.LENGTH_SHORT).show()
            checkForm.visibility = View.GONE
            checkSucces.visibility = View.VISIBLE
            toastRunnable.run()

            //  AccountToUpload(var accountName: String?, var accountEmail: String?, var accountPhone: String?, var accountFoundOutBy: String?, var accountImage: String?)

            if (phoneText.isEmpty()) {
                upLoadImage(nameText, emailText, "No mando numero", spinnerText)
            } else {
                upLoadImage(nameText, emailText, phoneText, spinnerText)
            }

        }else{
            if (nameText.equals("")){
                checkName.requestFocus()
                checkName.setError("Campo requerido")
            }
            if (emailText.equals("")){
                checkEmail.requestFocus()
                checkEmail.setError("Campo requerido")
            }
        }

    }

    override fun registerNewAccount() {
        showFragmentOne()
    }

    override fun changeFragment() {
        handler.removeCallbacks(toastRunnable)
        number = 0
    }

    override fun onFragmentInteraction() {
        showFragmentOne()
    }

    override fun showCollection() {
        showFragmentTwo()
    }

    private fun showFragmentOne() {
        val transaction = manager.beginTransaction()
        val fragment = FormFragment()
        transaction.replace(R.id.fragment_holder, fragment)
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.commit()
    }

    private fun showFragmentTwo() {
        val transaction = manager.beginTransaction()
        val fragment = CollectionViewFragment()
        transaction.replace(R.id.fragment_holder, fragment)
        transaction.addToBackStack("FormFragment")
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.commit()
    }


    fun changeScreen() {
        if (number == 4) {
            showFragmentTwo()
        }
    }

    fun upLoadImage( name: String, mail: String, number: String, founOutBy: String) {
        val uri = Uri.parse("android.resource://sv.edu.bitlab.desafio.gilberto/drawable/kotlin_image")
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(uri)
            .addOnSuccessListener { fileURL ->
                Log.d("StatusRegister", "Successfully uploaded image: ${fileURL.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d("UPLOAD", "File Location: $it")
                    imageURL = it.toString()
                    upLoadData(name, mail, number, founOutBy, "$it")
                }
            }
            .addOnFailureListener {
                Log.d(
                    "UPLOAD", "Bad uploaded image: ${it.message} ${it.localizedMessage}")
            }

    }

    fun upLoadData(accountName: String, accountEmail: String, accountPhone : String, accountFoundOutBy: String, accountImage: String) {
        val db = FirebaseFirestore.getInstance()
        val data = AccountToUpload(accountName, accountEmail, accountPhone, accountFoundOutBy, accountImage)

        // Add a new document with a generated ID
        db.collection("accounts")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d("UPLOAD", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("UPLOAD", "Error adding document", e)
            }
    }

    private val toastRunnable = object : Runnable {
        override fun run() {
            number++
            changeScreen()
//            Toast.makeText(this@MainActivity, "This is a delayed toast $number", Toast.LENGTH_SHORT).show()
            Log.d("STATUS_COUNTER", "$number")
            handler.postDelayed(this, 1000)

        }
    }
}
