package sv.edu.bitlab.desafio.gilberto

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class CollectionAdapter(val arrayList: ArrayList<AccountToDownload>, val context: Context) : RecyclerView.Adapter<CollectionAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_collection, parent, false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.accountName.text = arrayList[position].accountName
        holder.email.text = arrayList[position].accountEmail
        holder.phone.text = arrayList[position].accountPhone
        holder.founOutBy.text = arrayList[position].accountFoundOutBy
        Glide.with(context)
            .load(arrayList[position].accountImage)
            .into(holder.image)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val phone = view.findViewById<TextView>(R.id.number_textview)
        val image = view.findViewById<ImageView>(R.id.account_image)
        val email = view.findViewById<TextView>(R.id.email_textfield)
        val accountName = view.findViewById<TextView>(R.id.name_textview)
        val founOutBy = view.findViewById<TextView>(R.id.knowus_textview)

    }

}