package sv.edu.bitlab.desafio.gilberto

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import sv.edu.bitlab.desafio.gilberto.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CollectionViewFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CollectionViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CollectionViewFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private var dataArray: ArrayList<AccountToDownload> = arrayListOf()
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adapter: CollectionAdapter
    private lateinit var newRegister: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_collection_view, container, false)
        list = view.findViewById(R.id.fragment_collection_recycler)
        newRegister = view.findViewById(R.id.new_register_textview)
        layoutManager = LinearLayoutManager(context)
        adapter = CollectionAdapter(dataArray, context!!)

        list.layoutManager = layoutManager
        list.adapter = adapter

        showData()

        newRegister.setOnClickListener {
            listener?.registerNewAccount()
        }

        return  view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed() {
        listener?.onFragmentInteraction()
    }

    fun showData() {
        val db = FirebaseFirestore.getInstance()
        db.collection("accounts")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    dataArray.clear()
                    for (document in task.result!!) {
                        Log.d("DIFFERENTDOG", document.id + " => " + document.data)
                        val accountName = document.getString("accountName")
                        val accountEmail = document.getString("accountEmail")
                        val accountPhone = document.getString("accountPhone")
                        val accountFoundOutBy = document.getString("accountFoundOutBy")
                        val accountImage = document.getString("accountImage")

                        dataArray.add(AccountToDownload(accountName, accountEmail, accountPhone, accountFoundOutBy, accountImage))
                    }
                    adapter.notifyDataSetChanged()
                    Log.d("DATA_STATUS", "$dataArray")
                } else {
                    Log.w("DIFFERENTDOG", "Error getting documents.", task.exception)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction()
        fun registerNewAccount()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CollectionViewFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CollectionViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
