package sv.edu.bitlab.desafio.gilberto

data class AccountToUpload(var accountName: String?, var accountEmail: String?, var accountPhone: String?, var accountFoundOutBy: String?, var accountImage: String?)

data class AccountToDownload(var accountName: String?, var accountEmail: String?, var accountPhone: String?, var accountFoundOutBy: String?, var accountImage: String?)